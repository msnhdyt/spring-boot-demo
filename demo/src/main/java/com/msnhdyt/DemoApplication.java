package com.msnhdyt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import com.msnhdyt.model.*;

@SpringBootApplication
public class DemoApplication {
	
	private static final Logger logger = LoggerFactory.getLogger(DemoApplication.class);

	public static void main(String[] args) {
		logger.info("this is a info message");
		logger.warn("this is a warn message");
		logger.error("this is a error message");
		SpringApplication.run(DemoApplication.class, args);
	}
	
//	@RequestMapping("/getmhs")
//	public Mahasiswa getMahasiswa() {
//		Mahasiswa objMhs = new Mahasiswa();
//		objMhs.setId(1);
//		objMhs.setNama("John");
//		objMhs.setAlamat("Bandung");
//		objMhs.setDob("2022-12-02");
//		objMhs.setNim("12345");
//		return objMhs;
//	}

}
