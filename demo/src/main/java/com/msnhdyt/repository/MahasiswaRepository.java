package com.msnhdyt.repository;

import org.springframework.data.repository.CrudRepository;
import com.msnhdyt.model.Mahasiswa;

public interface MahasiswaRepository extends CrudRepository<Mahasiswa, Integer>{
	
}
